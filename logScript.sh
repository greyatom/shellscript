#!/bin/bash

USER=$(ls -1 /home | grep -v 'ubuntu')

/sbin/ausearch --input-logs -f "/home/$USER/" > /home/ubuntu/audit.log
time=''
#echo $USER

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ $line == \time* ]] ; then
        time=${line/time->/}
    fi
done < "/home/ubuntu/audit.log"
time=${time// /_}
#curl http://api.greyatom.com/v2/aws/cronCallBack/"$time"/"$USER"
curl http://api2.commit.live/v2/aws/cronCallBack/"$time"/"$USER"
echo "" > /home/ubuntu/audit.log