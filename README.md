SHELL SCRIPT FOR AUDITING TO FIND IDLE INSTANCE
This uses the tools from auditd & audispd-plugins
Cron should be setup on this script every 10 minutes (or so) OR run manually for one time log.
crontab -l (List crontab if already added)
crontab -e
*/10 * * * * /home/ubuntu/shellscript/logScript.sh